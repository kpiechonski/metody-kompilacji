%{
#include <string>
#include <stdio.h>
#include <stack>
#include <map>
#include <vector>
#include <iostream>
#include <fstream>

#define INFILE_ERROR 1
#define OUTFILE_ERROR 2
FILE * yyhandle;
extern "C" int yylex();
extern "C" int yyerror(const char *msg, ...);
using namespace std;

class Symbol
{
public:
	Symbol() { }

	Symbol(string value, string type)
	{
		this->type = type;
		this->kind = "IDENTIFIER";
		this->value = value;
		this->sizes.push_back(1);
	}

	void setSizes(vector<int> sizes)
	{
		this->sizes = sizes;

		this->multipliers.push_back(4);

		for(auto i = this->sizes.rbegin(); i != prev(this->sizes.rend()); i++)
		{
			this->multipliers.push_back(this->multipliers.back() * (*i));
		}
	}

	string kind;
	string type;
	string value;
	vector<int> sizes;
	vector<int> multipliers;
};

stack<Symbol> symbols;
stack<string> conditionLabels;
stack<string> conditionOperators;
map<string, Symbol> identifiers;
vector<int> currentArraySizes;

vector<string> dotdata;
vector<string> dottext;

int tmpCounter = 0;
int labelCounter = 0;

void addId(Symbol id)
{
	identifiers[id.value] = id;
	
	int size = 1;

	for(auto i : id.sizes)
	{
		size *= i;
	}

	string type = id.type == "int" ? "word" : "float";
	dotdata.push_back(id.value + ": ." + type + " 0:" + to_string(size));
}

void load(Symbol what, string where)
{
	if(what.kind == "CONSTANT" && what.type == "float")
	{
		Symbol fwhat(string("$$tmp") + to_string(tmpCounter++), "float");
		dotdata.push_back(fwhat.value + ": .float " + what.value);
		what = fwhat;
	}

	string instruction;
	instruction.append("l");

	if(what.kind == "IDENTIFIER")
	{
		if(what.type == "int")
			instruction.append("w");
		else if(what.type == "float")
			instruction.append(".s");
	}
	else if(what.kind == "CONSTANT")
	{
		instruction.append("i");
	}

	instruction.append(" ");
	instruction.append(where);
	instruction.append(", ");
	instruction.append(what.value);

	dottext.push_back(instruction);
}

void calculate(string operation)
{
	Symbol a, b;
	a = symbols.top();
	symbols.pop();
	b = symbols.top();
	symbols.pop();

	if(a.type != b.type) {
		yyerror("error: variables are of different type\n");
	}
	
	Symbol w;
	w.kind = "IDENTIFIER";
	w.type = a.type;
	w.value = "$$tmp" + to_string(tmpCounter++);
	symbols.push(w);

	addId(w);

	if(a.type == "int")
	{
		load(a, "$t0");
		load(b, "$t1");
		dottext.push_back(operation + " $t0,$t0,$t1");
		dottext.push_back(string("sw $t0, " + w.value));
	}
	else if(a.type == "float")
	{
		load(a, "$f0");
		load(b, "$f1");
		dottext.push_back(operation + ".s $f0,$f0,$f1");
		dottext.push_back(string("s.s $f0, " + w.value));
	}
}



%}
%union 
{char *text;
float fval;
int	ival;};
%type <text> expression1
%token <text> IDENTIFIER
%token <ival> CONSTANT
%token <fval> FLOAT_CONSTANT

%token STRING_LITERAL 
%token INC_OP DEC_OP LEFT_OP RIGHT_OP LE_OP GE_OP EQ_OP NE_OP
%token AND_OP OR_OP MUL_ASSIGN DIV_ASSIGN MOD_ASSIGN ADD_ASSIGN
%token SUB_ASSIGN LEFT_ASSIGN RIGHT_ASSIGN AND_ASSIGN
%token XOR_ASSIGN OR_ASSIGN

%token INT FLOAT VOID STRING

%token ARRAY ASSIGN FUNCTION

%token IF ELSE FOR RETURN

%token PRINT_INT READ_INT PRINT_FLOAT READ_FLOAT

%%
all
	: instructions
	;

instructions
	: instruction
	| instructions instruction
	;	

instruction
	: declaration';'
	| assignment';'
	| condition block else_keyword block {
		dottext.push_back(conditionLabels.top() + ":");
		conditionLabels.pop();
	}
	| for_keyword '(' IDENTIFIER ';' CONSTANT ';' CONSTANT ')' '{' instructions '}' {
		if ( identifiers.find($3) == identifiers.end() ) {
			string error = "error: undefined variable ";
			error.append($3);
			error.append("\n");
			yyerror(error.c_str());
		}

		Symbol iterator = identifiers.find($3)->second;

		if(iterator.type != "int") {
			yyerror("iterator must be int\n");
		}

		Symbol step;
		step.value = to_string($5);
		step.type = "int";
		step.kind = "CONSTANT";

		Symbol max;
		max.value = to_string($7);
		max.type = "int";
		max.kind = "CONSTANT";

		load(iterator, "$t0");
		load(step, "$t1");
		load(max, "$t2");

		dottext.push_back("add $t0, $t0, $t1");
		dottext.push_back(string("sw $t0, ") + iterator.value);
		dottext.push_back(string("blt $t0, $t2, ") + conditionLabels.top());
		conditionLabels.pop();
	}
	| condition block {
		dottext.push_back(string("ELSE_") + conditionLabels.top() + ":");
		dottext.push_back(conditionLabels.top() + ":");
		conditionLabels.pop();
	}
	| PRINT_INT expression1';' {
		dottext.push_back("li $v0, 1");
		load(symbols.top(), "$a0");
		symbols.pop();
		dottext.push_back("syscall");
	}
	| READ_INT IDENTIFIER';' {
		dottext.push_back("li $v0, 5");
		dottext.push_back("syscall");
		dottext.push_back(string("sw $v0, ") + $2);
	}
	| PRINT_FLOAT expression1';' {
		dottext.push_back("li $v0, 2");
		load(symbols.top(), "$f12");
		symbols.pop();
		dottext.push_back("syscall");
	}
	| READ_FLOAT IDENTIFIER';' {
		dottext.push_back("li $v0, 6");
		dottext.push_back("syscall");
		dottext.push_back(string("s.s $f0, ") + $2);
	}
	;
for_keyword
	: FOR  {
		conditionLabels.push(string("FOR_LABEL") + to_string(labelCounter++));
		dottext.push_back(conditionLabels.top() + ":");
	}
	;

condition
	: IF '(' expression1 conditional_operator expression1 ')' {
		conditionLabels.push(string("END_IF_LABEL") + to_string(labelCounter++));

		load(symbols.top(), "$t1");
		symbols.pop();
		load(symbols.top(), "$t0");
		symbols.pop();

		dottext.push_back(conditionOperators.top() + " " + "$t0, $t1, " + conditionLabels.top());
	}
	;

conditional_operator
	: '<'	{ conditionOperators.push("bge"); }
	| '>'	{ conditionOperators.push("ble"); }
	| LE_OP { conditionOperators.push("bgt"); }
	| GE_OP { conditionOperators.push("blt"); }
	| EQ_OP { conditionOperators.push("bne"); }
	| NE_OP { conditionOperators.push("beq"); }
	;
else_keyword
	: ELSE {
		dottext.push_back(string("ELSE_") + conditionLabels.top() + ":");
	}
	;

block
	: '{' instructions '}' {
		dottext.push_back(string("j ") + conditionLabels.top());
	}
	;

assignment
	: IDENTIFIER array_access '=' expression1 {

		if ( identifiers.find($1) == identifiers.end() ) {
			string error = "error: undefined variable ";
			error.append($1);
			error.append("\n");
			yyerror(error.c_str());
		}

		Symbol id = identifiers.find($1)->second;
		Symbol rhs = symbols.top();
		symbols.pop();

		if(id.type != rhs.type) {
			yyerror("error: variables are of different type\n");
		}

		if(id.type == "int")
		{
			dottext.push_back(string("la $t5, ") + id.value);

			int shift = 0;

			for(auto multiplier = id.multipliers.begin(); !symbols.empty(); multiplier++)
			{
				auto access = symbols.top();
				symbols.pop();
				
				load(access, "$t0");
				dottext.push_back(string("li $t1, ") + to_string(*multiplier));
				dottext.push_back("mul $t0, $t0, $t1");
				dottext.push_back("add $t5, $t5, $t0");
			}

			load(rhs, "$t0");
			dottext.push_back("sw $t0, ($t5)");
		}
		else
		{
			dottext.push_back(string("la $t5, ") + id.value);

			int shift = 0;

			for(auto multiplier = id.multipliers.begin(); !symbols.empty(); multiplier++)
			{
				auto access = symbols.top();
				symbols.pop();
				
				load(access, "$t0");
				dottext.push_back(string("li $t1, ") + to_string(*multiplier));
				dottext.push_back("mul $t0, $t0, $t1");
				dottext.push_back("add $t5, $t5, $t0");
			}

			load(rhs, "$f0");
			dottext.push_back("s.s $f0, ($t5)");
		}
		
	}
	| IDENTIFIER '=' expression1 {
		if ( identifiers.find($1) == identifiers.end() ) {
			string error = "error: undefined variable ";
			error.append($1);
			error.append("\n");
			yyerror(error.c_str());
		}

		if(symbols.top().type == "int")
		{
			load(symbols.top(), "$t0");
			dottext.push_back(string("sw $t0, ") + $1);
		}
		else if(symbols.top().type == "float")
		{
			load(symbols.top(), "$f0");
			dottext.push_back(string("s.s $f0, ") + $1);
		}

		symbols.pop();
	}
	;

declaration
	: ARRAY '(' INT array_sizes IDENTIFIER {

		Symbol id($5, "int");
		id.setSizes(currentArraySizes);

		currentArraySizes.clear();
		addId(id);
	}
	| ARRAY '(' FLOAT array_sizes IDENTIFIER {

		Symbol id($5, "float");
		id.setSizes(currentArraySizes);

		currentArraySizes.clear();
		addId(id);
	}
	| INT IDENTIFIER {
		addId(Symbol($2, "int"));
	}
	| FLOAT IDENTIFIER {
		addId(Symbol($2, "float"));
	}
	| STRING IDENTIFIER {
		addId(Symbol($2, "string"));
	}
	;

array_sizes
	: ',' array_size ')'
	| ',' array_size array_sizes
	;

array_size
	: CONSTANT  { currentArraySizes.push_back($1); }
	;

array_access
	: '[' expression1 ']'
	| '[' expression1 ']' array_access
	;

expression1
	: expression1 '+' expression2	{
		calculate("add");
	}
	| expression1 '-' expression2	{
		calculate("sub");
	}
	| expression2		{;}
	;
expression2
	: expression2 '*' czynnik	{
		calculate("mul");
	}
	| expression2 '/' czynnik	{
		calculate("div");
	}
	| czynnik		{;}
	;
czynnik
	: IDENTIFIER array_access {

		Symbol id = identifiers.find($1)->second;

		dottext.push_back(string("la $t5, ") + id.value);

		int shift = 0;

		for(auto multiplier = id.multipliers.begin(); !symbols.empty(); multiplier++)
		{
			auto access = symbols.top();
			symbols.pop();
			
			load(access, "$t0");
			dottext.push_back(string("li $t1, ") + to_string(*multiplier));
			dottext.push_back("mul $t0, $t0, $t1");
			dottext.push_back("add $t5, $t5, $t0");
		}

		Symbol w;
		w.kind = "IDENTIFIER";
		w.type = id.type;
		w.value = "$$tmp" + to_string(tmpCounter++);
		symbols.push(w);

		addId(w);		

		dottext.push_back("lw $t0, ($t5)");

		dottext.push_back(string("sw $t0, ") + w.value);
		dottext.push_back(string());
	}
	| IDENTIFIER			{
		if ( identifiers.find($1) == identifiers.end() ) {
			string error = "error: undefined variable ";
			error.append($1);
			error.append("\n");
			yyerror(error.c_str());
		}

		Symbol id = identifiers.find($1)->second;

		symbols.push(id);
	}
	| CONSTANT			{
		//fprintf(yyhandle, " %d ", $1);
		Symbol symbol;
		symbol.kind = "CONSTANT";
		symbol.type = "int";
		symbol.value = to_string($1);

		symbols.push(symbol);
	}
	| FLOAT_CONSTANT			{
		//fprintf(yyhandle, " %d ", $1);
		Symbol symbol;
		symbol.kind = "CONSTANT";
		symbol.type = "float";
		symbol.value = to_string($1);

		symbols.push(symbol);
	}
	|'(' expression1 ')'		{;}
	;
%%


int main(int argc, char *argv[])
{
	yyhandle = fopen("trojki.txt", "w");
	yyparse();

	ofstream mars;
	mars.open("mars.asm");

	mars << ".data\n";

	for(auto i : dotdata)
	{
		mars << "\t" << i << "\n";
	}

	mars << "\n.text\n\tmain:\n";

	for(auto i : dottext)
	{
		mars << "\t" << i << "\n";
	}

	mars.close();
	return 0;
}
