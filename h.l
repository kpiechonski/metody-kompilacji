%{
#include <stdlib.h>
#include <string.h>
#include "def.tab.hh"
#define INFILE_ERROR 1
#define OUTFILE_ERROR 2
extern int yylineno;
int yyerror(const char *,...);
void writeLexValue(char *);

// based on C grammar:
// http://www.lysator.liu.se/c/ANSI-C-grammar-l.html
%}
%%
"void"						{ writeLexValue("VOID"); return VOID; }
"int"						{ writeLexValue("INT"); return INT; }
"float"						{ writeLexValue("FLOAT"); return FLOAT; }
"string"					{ writeLexValue("STRING"); return STRING; }

"array"						{ writeLexValue("ARRAY"); return ARRAY; }
"assign"					{ writeLexValue("ASSIGN"); return ASSIGN; }

"if"						{ writeLexValue("IF"); return IF; }
"else"						{ writeLexValue("ELSE"); return ELSE; }
"for"						{ writeLexValue("FOR"); return FOR; }
"function"					{ writeLexValue("FUNCTION"); return FUNCTION; }
"return"					{ writeLexValue("RETURN"); return RETURN; }

"print_int"					{ writeLexValue("PRINT_INT"); return PRINT_INT; }
"read_int"					{ writeLexValue("READ_INT"); return READ_INT; }
"print_float"				{ writeLexValue("PRINT_FLOAT"); return PRINT_FLOAT; }
"read_float"				{ writeLexValue("READ_FLOAT"); return READ_FLOAT; }

0x[a-fA-F0-9]+				{ writeLexValue("CONSTANT"); yylval.ival = atoi(yytext); return CONSTANT; }
[0-9]+						{ writeLexValue("CONSTANT"); yylval.ival = atoi(yytext); return CONSTANT; }
[0-9]+"."[0-9]+				{ writeLexValue("FLOAT_CONSTANT"); yylval.fval = atof(yytext); return FLOAT_CONSTANT; }
\"(\\.|[^\\"])*\"			{ writeLexValue("STRING_LITERAL"); return STRING_LITERAL; }

[a-zA-Z_]([a-zA-Z_0-9])*	{ writeLexValue("IDENTIFIER"); yylval.text = strdup(yytext); return IDENTIFIER; }

">>="						{ writeLexValue("RIGHT_ASSIGN"); return RIGHT_ASSIGN; }
"<<="						{ writeLexValue("LEFT_ASSIGN"); return LEFT_ASSIGN; }
"+="						{ writeLexValue("ADD_ASSIGN"); return ADD_ASSIGN; }
"-="						{ writeLexValue("SUB_ASSIGN"); return SUB_ASSIGN; }
"*="						{ writeLexValue("MUL_ASSIGN"); return MUL_ASSIGN; }
"/="						{ writeLexValue("DIV_ASSIGN"); return DIV_ASSIGN; }
"%="						{ writeLexValue("MOD_ASSIGN"); return MOD_ASSIGN; }
"&="						{ writeLexValue("AND_ASSIGN"); return AND_ASSIGN; }
"^="						{ writeLexValue("XOR_ASSIGN"); return XOR_ASSIGN; }
"|="						{ writeLexValue("OR_ASSIGN"); return OR_ASSIGN; }
">>"						{ writeLexValue("RIGHT_OP"); return RIGHT_OP; }
"<<"						{ writeLexValue("LEFT_OP"); return LEFT_OP; }
"++"						{ writeLexValue("INC_OP"); return INC_OP; }
"--"						{ writeLexValue("DEC_OP"); return DEC_OP; }
"&&"						{ writeLexValue("AND_OP"); return AND_OP; }
"||"						{ writeLexValue("OR_OP"); return OR_OP; }
"<="						{ writeLexValue("LE_OP"); return LE_OP; }
">="						{ writeLexValue("GE_OP"); return GE_OP; }
"=="						{ writeLexValue("EQ_OP"); return EQ_OP; }
"!="						{ writeLexValue("NE_OP"); return NE_OP; }
";"							{ writeLexValue("';'"); return ';'; }
"{"							{ writeLexValue("'{'"); return '{'; }
"}"							{ writeLexValue("'}'"); return '}'; }
","							{ writeLexValue("','"); return ','; }
"="							{ writeLexValue("'='"); return '='; }
"("							{ writeLexValue("'('"); return '('; }
")"							{ writeLexValue("')'"); return ')'; }
"["							{ writeLexValue("'['"); return '['; }
"]"							{ writeLexValue("']'"); return ']'; }
"&"							{ writeLexValue("'&'"); return '&'; }
"!"							{ writeLexValue("'!'"); return '!'; }
"-"							{ writeLexValue("'-'"); return '-'; }
"+"							{ writeLexValue("'+'"); return '+'; }
"*"							{ writeLexValue("'*'"); return '*'; }
"/"							{ writeLexValue("'/'"); return '/'; }
"%"							{ writeLexValue("'%'"); return '%'; }
"<"							{ writeLexValue("'<'"); return '<'; }
">"							{ writeLexValue("'>'"); return '>'; }
"^"							{ writeLexValue("'^'"); return '^'; }
"|"							{ writeLexValue("'|'"); return '|'; }

[ \t\v\n\f]					{  }
.							{yyerror("Błąd leksykalny\n");}
%%
int yyerror(const char *msg,...)
{
	printf("%d: %s", yylineno, msg);
	exit(1);
}

void writeLexValue(char *value)
{
	fprintf(yyout, "%d::%s\n", yylineno, value);
}

